# tox quo vadis?

Materials from the badly prepared, sloppily held lightning talk at EuroPython2018.

#### [watch the video](https://youtu.be/7uSjCp1wOrw?t=9h4m50s) || [view slides (via githack)](https://glcdn.githack.com/obestwalter/tox-lightning-talk/raw/master/tox-quo-vadis.html) || [ci job output](https://gitlab.com/obestwalter/tox-lightning-talk/-/jobs/85012663)

See (and share) also my [Twitter thread in the same vein](https://twitter.com/obestwalter/status/1002900635396857856).

![screen.png](screen.png)

## TL;DW

**tox is not just for testing against many versions of Python - it's the main entry point for all development task automation.**
